package jasonlow.myfruitsdiary;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainNavigationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_navigation);
    }
}
